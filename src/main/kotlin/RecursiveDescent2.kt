import java.io.FileReader
import java.util.*

data class Person3(val name : String,
                   val age : Int,
                   val addresses : MutableMap<String, Address3> = mutableMapOf())
data class Address3(val street: String,
                    val city: String,
                    val state: String)

enum class TokenType {
    EOF, WAITING, ID, NAME, AGE, PERSON, STRING, INT, ADDRESS, STREET, CITY, STATE, OPEN_CURLY, CLOSE_CURLY
}
class RecursiveDescent2 {
    companion object {
        val WAITING = Token(TokenType.WAITING, "WAITING")
        val regex = "[^\"\\s]+|\"(\\\\.|[^\\\\\"])*\""

        var LA1 : Token = WAITING
        @JvmStatic
        fun main(args: Array<String>) {
            Scanner(FileReader("data.people")).use {scanner ->
// WAS ONLY FOR TESTING THE SCANNER...
//                while(true) {
//                    val token = nextToken(scanner)
//                    println(token)
//                    if (token.code == TokenType.EOF) {
//                        break
//                    }
//                }
                val people = mutableMapOf<String, Person3>()
                stuff(scanner, people)
                println(people)
            }
        }

// stuff
//  :   "name" STRING
//      "age" INT
//      person*
//  ;

        fun stuff(scanner: Scanner, people : MutableMap<String, Person3>) {
            consume(scanner, TokenType.NAME)
            val name = consume(scanner, TokenType.STRING).text
            consume(scanner, TokenType.AGE)
            val age = consume(scanner, TokenType.INT).text.toInt()
            println(name)
            println(age)
            while(match(scanner, TokenType.PERSON)) {
                person(scanner, people)
            }
        }

//  person
//  :   "person" ID "{"
//          "name" STRING
//          "age" INT
//          address*
//      "}"
//  ;
        private fun person(scanner: Scanner, people: MutableMap<String, Person3>) {
            consume(scanner, TokenType.PERSON)
            val id = consume(scanner, TokenType.ID).text
            consume(scanner, TokenType.OPEN_CURLY)
            consume(scanner, TokenType.NAME)
            val name = consume(scanner, TokenType.STRING).text
            consume(scanner, TokenType.AGE)
            val age = consume(scanner, TokenType.INT).text.toInt()

            val person = Person3(name, age)
            people[id] = person
            while(match(scanner, TokenType.ADDRESS)) {
                address(scanner, person)
            }
            consume(scanner, TokenType.CLOSE_CURLY)
        }

//  address
//  :   "address" ID "{"
//          "street" STRING
//          "city" STRING
//          "state" STRING
//      "}"

/*

A   :   "x" aaa | "y" bbb
 */

        private fun address(scanner: Scanner, person: Person3) {
            consume(scanner, TokenType.ADDRESS)
            val id = consume(scanner, TokenType.ID).text
            consume(scanner, TokenType.OPEN_CURLY)
            var done = false
            var street : String? = null
            var city : String? = null
            var state : String? = null
            while(!done) {
                when {
                    match(scanner, TokenType.STREET) -> {
                        consume(scanner, TokenType.STREET)
                        street = street?.apply {
                            throw IllegalArgumentException("street more than once")
                        } ?: consume(scanner, TokenType.STRING).text
                    }
                    match(scanner, TokenType.CITY) -> {
                        consume(scanner, TokenType.CITY)
                        city = city?.apply {
                            throw IllegalArgumentException("city more than once")
                        } ?: consume(scanner, TokenType.STRING).text
                    }
                    match(scanner, TokenType.STATE) -> {
                        consume(scanner, TokenType.STATE)
                        state = state?.apply {
                            throw IllegalArgumentException("state more than once")
                        } ?: consume(scanner, TokenType.STRING).text
                    }
                    else -> {done = true}
                }
            }

            if (street == null || city == null || state == null) {
                throw IllegalArgumentException("street, city, and state must be specified!")
            }
            val address = Address3(street, city, state)
            person.addresses[id] = address
            consume(scanner, TokenType.CLOSE_CURLY)
        }


        fun match(scanner: Scanner, code : TokenType) : Boolean {
            if (LA1 == WAITING) {
                LA1 = nextToken(scanner)
            }
            return LA1.code == code
        }

        fun consume(scanner: Scanner, code : TokenType) : Token {
            if (match(scanner, code)) {
//                return LA1.apply {LA1 = WAITING}
                val token = LA1
                LA1 = WAITING
                return token
            }
            throw IllegalArgumentException("unexpected $LA1; expected token type $code")
        }

        fun nextToken(scanner: Scanner) : Token {
            var word : String? = null
            while(true) {
                word = scanner.findWithinHorizon(regex, 0)
                println(word)
                if (word == null || word.isNotBlank()) {
                    break
                }
            }
            return when {
                word == null -> Token(TokenType.EOF, "<eof>")
                word == "{" -> Token(TokenType.OPEN_CURLY, word)
                word == "}" -> Token(TokenType.CLOSE_CURLY, word)
                word == "person" -> Token(TokenType.PERSON, word)
                word == "name" -> Token(TokenType.NAME, word)
                word == "age" -> Token(TokenType.AGE, word)
                word == "address" -> Token(TokenType.ADDRESS, word)
                word == "street" -> Token(TokenType.STREET, word)
                word == "city" -> Token(TokenType.CITY, word)
                word == "state" -> Token(TokenType.STATE, word)
                word.first() == '"' -> Token(TokenType.STRING, word.trim('"'))
                word.toIntOrNull() != null -> Token(TokenType.INT, word)
                else -> Token(TokenType.ID, word)
            }
        }
    }
}

data class Token(val code : TokenType, val text : String)
