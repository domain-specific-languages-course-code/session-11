import java.io.FileReader
import java.io.FileWriter
import java.util.*


class Properties1 {
    var x = 42
    var name = "Scott"
    var list = mutableListOf(
            "A", "B", "C", "D"
    )

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val thing = Properties1()
            thing.write("data.properties")
            thing.read("data.properties")
        }
    }

    fun read(fileName : String) {
        val properties = Properties()
        FileReader(fileName).use {fr ->
            properties.load(fr)
        }
        x = properties.getProperty("x").toInt()
        name = properties.getProperty("name")
        val numEntries = properties.getProperty("numEntries").toInt()
        list = ArrayList(numEntries)
        (0 until numEntries).forEach {n ->
            list.add(properties.getProperty("list.$n"))
        }

        println(x)
        println(name)
        println(list)
    }
    fun write(fileName : String) {
        val properties = Properties()
        properties["x"] = x.toString()
        properties["name"] = name
        properties["numEntries"] = list.size.toString()
        list.forEachIndexed {n, entry ->
            properties["list.$n"] = entry
        }
        FileWriter(fileName).use {fw ->
            properties.store(fw, "Sample data")
        }
    }
}