import javax.swing.*;

public class Foot {
    public static void main(String[] args) {
        A a = new A();
        System.out.println(a.x);
        System.out.println(a.y);
        new JFrame() {{
           add(new JPanel() {{
                   add(new JButton() {{
                       addActionListener(e -> {
                           // do thing
                       });
                   }});
           }});
        }};
    }
}
class A {
    int x = 10;
    {
        x = 42;
    }

    int y = x;
    {
        y = 22;
    }
}