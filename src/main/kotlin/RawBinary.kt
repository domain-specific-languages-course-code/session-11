import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.FileInputStream
import java.io.FileOutputStream

class RawBinary {
    var x = 42
    var name = "Scott"
    var list = mutableListOf(
            "A", "B", "C", "D"
    )

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val thing = RawBinary()
            thing.write("data.bin")
            thing.read("data.bin")
        }
    }
    fun read(fileName : String) {
        DataInputStream(FileInputStream(fileName)).use {dis ->
            x = dis.readInt()
            name = dis.readUTF()
            val numEntries = dis.readInt()
            list = ArrayList(numEntries)
            (0 until numEntries).forEach {
                list.add(dis.readUTF())
            }
        }
        println(x)
        println(name)
        println(list)
    }
    fun write(fileName : String) {
        DataOutputStream(FileOutputStream(fileName)).use { dos ->
            dos.writeInt(x)
            dos.writeUTF(name)
            dos.writeInt(list.size)
            list.forEach {
                dos.writeUTF(it)
            }
        }
    }
}