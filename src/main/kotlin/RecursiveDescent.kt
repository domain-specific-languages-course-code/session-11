import java.io.FileReader
import java.util.*

data class Person2(val name : String, val age : Int, val addresses : MutableMap<String, Address2> = mutableMapOf())
data class Address2(val street : String, val city : String, val zip : String)

class RecursiveDescent {
    companion object {
        const val EOF = 1
        const val NAME = 2
        const val AGE = 3
        const val PERSON = 4
        const val STRING = 5
        const val INT = 6
        const val ID = 7
        const val OPEN_CURLY = 8
        const val CLOSE_CURLY = 9
        const val ADDRESS = 10
        const val STREET = 11
        const val CITY = 12
        const val STATE = 13

        val WAITING = Token(0, "")

        var LA1 : Token = WAITING

        val regex = "[^\"\\s]+|\"(\\\\.|[^\\\\\"])*\""

//        val regex2 = """[^"\s]+|"(.|[^"])*"""".toPattern()

        @JvmStatic
        fun main(args: Array<String>) {
            Scanner(FileReader("data.people")).use { scanner ->
//                scanner.useDelimiter("""\s+""".toPattern())
                val people = mutableMapOf<String, Person2>()
                parse(scanner, people)
                println(people)
            }
        }

/*

parse
    :   "name" STRING
        "age" INT
        person*
    ;

person
    :   "person" ID "{"
            "name" STRING
            "age" INT
            address*
        "}"
    ;

address
    :   "address" ID "{"
            "street" STRING
            "city" STRING
            "state" STRING
        "}"

 */


        fun parse(scanner : Scanner, people : MutableMap<String, Person2>) {
            consume(scanner, NAME)
            val name = consume(scanner, STRING).text
            consume(scanner, AGE)
            val age = consume(scanner, INT).text.toInt()
            println("name=$name")
            println("age=$age")
            while(match(scanner, PERSON)) {
                person(scanner, people)
            }
            println(people)
        }

        fun person(scanner: Scanner, people : MutableMap<String, Person2>) {
            consume(scanner, PERSON)
            val id = consume(scanner, ID)
            consume(scanner, OPEN_CURLY)
            consume(scanner, NAME)
            val name = consume(scanner, STRING)
            consume(scanner, AGE)
            val age = consume(scanner, INT)

            val person = Person2(name.text, age.text.toInt())
            people[id.text] = person

            while (match(scanner, ADDRESS)) {
                address(scanner, person)
                // NOTE: NO CONSUME HERE!!! JUST LOOK AHEAD
            }
            consume(scanner, CLOSE_CURLY)
        }

        fun address(scanner: Scanner, person : Person2) {
            consume(scanner, ADDRESS)
            val id = consume(scanner, ID)
            consume(scanner, OPEN_CURLY)

            consume(scanner, STREET)
            val street = consume(scanner, STRING)
            consume(scanner, CITY)
            val city = consume(scanner, STRING)
            consume(scanner, STATE)
            val state = consume(scanner, STRING)

            val address = Address2(street.text, city.text, state.text)
            person.addresses[id.text] = address

            consume(scanner, CLOSE_CURLY)
        }

        fun match(scanner: Scanner, code : Int) : Boolean {
            if (LA1 == WAITING) {
                LA1 = nextToken(scanner)
            }
            return LA1.code == code
        }

        fun consume(scanner: Scanner, code : Int) : Token {
            if (match(scanner, code)) {
                val token = LA1
                LA1 = WAITING
                return token
                // shorter...
                //    return LA1.apply { LA1 = WAITING }
            } else {
                throw IllegalArgumentException("unexpected $LA1; expected token type $code")
            }
        }

        data class Token(val code : Int, val text : String)

        fun nextToken(scanner: Scanner) : Token {
            var word : String?
            while(true) {
                try {
//                    word = scanner.next()
                    word = scanner.findWithinHorizon(regex, 0)
                } catch (e : NoSuchElementException) {
                    return Token(EOF, "<eof>")
                }
                println(word)
                if (word == null || word.isNotBlank()) break
            }

            return when {
                word == null -> Token(EOF, "<eof>")
                word == "{" -> Token(OPEN_CURLY, word)
                word == "}" -> Token(CLOSE_CURLY, word)
                word == "name" -> Token(NAME, word)
                word == "age" -> Token(AGE, word)
                word == "person" -> Token(PERSON, word)
                word == "address" -> Token(ADDRESS, word)
                word == "street" -> Token(STREET, word)
                word == "city" -> Token(CITY, word)
                word == "state" -> Token(STATE, word)
                word.first() == '"' -> Token(STRING, word.trim('"'))
                word.toIntOrNull() != null -> Token(INT, word)
                else -> Token(ID, word)
            }
        }
    }

}

