import org.json.JSONArray
import org.json.JSONObject
import java.io.File

data class Person(var name : String? = null,
                  var age : Int? = null)

class JSON1 {
    var x = 42
    var name = "Scott"
    var list = mutableListOf(
            Person("Scott", 51),
            Person("Ted", 22),
            Person("Wilford", 99)
    )

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val thing = JSON1()
            thing.write("data.json")
            thing.read("data.json")
        }
    }

    fun read(fileName : String) {
        // only use this for small-ish files...
        val contents = File(fileName).readText()
        val obj = JSONObject(contents)
        val array = obj.getJSONArray("list")
        x = obj.getInt("x")
        name = obj.getString("name")
        list = ArrayList(array.length())
        (0 until array.length()).forEach {n ->
            val item = array.getJSONObject(n)
            list.add(Person(item.getString("name"), item.getInt("age")))
        }

        println(x)
        println(name)
        println(list)
    }

    fun write(fileName : String) {
        val obj = JSONObject()
        obj.put("name", name)
        obj.put("x", x)
        val array = JSONArray()
        list.forEach { person->
            val pObj = JSONObject()
            pObj.put("name", person.name)
            pObj.put("age", person.age)
            array.put(pObj)
        }
        obj.put("list", array)
        File(fileName).writeText(obj.toString(4))
    }
}