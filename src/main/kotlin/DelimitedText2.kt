import java.io.BufferedReader
import java.io.PrintWriter
import java.io.File
import java.io.FileReader


class DelimitedText2 {
    var x = 42
    var name = "Scott"
    var list = mutableListOf(
            "A", "B", "C", "D"
    )

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val thing = DelimitedText2()
            thing.write("delimited2.txt")
            thing.read("delimited2.txt")
        }
    }

    fun read(fileName : String) {
        BufferedReader(FileReader(fileName)).use {br ->
            x = br.readLine().toInt()
            name = br.readLine()
            val numEntries = br.readLine().toInt()
            (0 until numEntries).forEach {
                list.add(br.readLine())
            }
        }

        println(x)
        println(name)
        println(list)
    }
    fun write(fileName : String) {
        PrintWriter(fileName).use {pw ->
            pw.println(x.toString())
            pw.println(name)
            pw.println(list.size.toString())
            list.forEach {
                pw.println(it)
            }
        }
    }
}