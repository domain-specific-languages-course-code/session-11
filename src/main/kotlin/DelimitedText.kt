import java.io.PrintWriter
import java.io.File


class DelimitedText {
    var x = 42
    var name = "Scott"
    var list = mutableListOf(
            "A", "B", "C", "D"
    )

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val thing = DelimitedText()
            thing.write("delimited.txt")
            thing.read("delimited.txt")
        }
    }

    fun read(fileName : String) {
        // only use this for small-ish files...
        val contents = File(fileName).readText()
        val chunks = contents.split("|")
        x = chunks[0].toInt()
        name = chunks[1]
        val numEntries = chunks[2].toInt()
        list = ArrayList(numEntries)
        (0 until numEntries).forEach {n ->
            list.add(chunks[n+3])
        }

        println(x)
        println(name)
        println(list)
    }
    fun write(fileName : String) {
        PrintWriter(fileName).use {pw ->
            pw.print(x.toString())
            pw.print("|")
            pw.print(name)
            pw.print("|")
            pw.print(list.size.toString())
            pw.print("|")
            list.forEach {
                pw.print(it)
                pw.print("|")
            }
        }
    }
}